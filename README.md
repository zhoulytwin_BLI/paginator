# paginator
This module implements pagination in a single component.
To use this module, pass in:
 pageHeight
 pageWidth as numbers (unit: px)
 renderers which is an array of callback functions to
   receive value array, and start and end array index and
   to return components to render
 and data which is an array of the same dimention with renderers. 
   this array contains nested array of values. Correcponding nested 
   array (rowGroup) will be rendered by correcponding renderer.
 e.g. 
   <Pager renderers={renderers}
          pageHeight={400}
          pageWidth={800}
          data={data}
   />

The component received renderers and data in such format because it
 seems easy to get data in that format. In ternally, the data is
 transformed to structured that ease manipulation.
 e.g. when slicing the rows to find the max number of rows that fit in
 a page, the data, as an array of arrays, is transformed to array of
 individual row ({pageId, groupId, rowId}). When rendering, the array
 of rows is converted to an array of array of rowGroup ({groupId, startRowId, 
 endRowId}), where the outer array represent pages and the inner represent
 rowGroups in a single page.
For performance, the component implements binary search and debouncing.
 */

# Improvements
1.
2. Consider using PureComponent. This component does not need to render if props do no change.
3. Shorten debounce time may help to give an impression of responsiveness.
4. Update heuristics for proper page height (unbalanced binary search)

# Usage
```
<Pager renderers={renderers}
       pageHeight={400}
       pageWidth={800}
       data={data}
/>
```
# Test
1. clone the repo
2. npm install
3. npm run start; this will start a test server at port 3000
4. play with App.jsx

# Import
This repo is npm installable, but not tested.
You should be able to `npm install git://OUR_BITBUCKET_URL/THIS_REPO`
Or simply, you can copy the src/lib/paginator to your project,
and import from there.

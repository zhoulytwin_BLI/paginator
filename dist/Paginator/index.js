/* This module implements pagination in a single component.
 * To use this module, pass in:
 *  pageHeight
 *  pageWidth as numbers (unit: px)
 *  renderers which is an array of callback functions to
 *    receive value array, and start and end array index and
 *    to return components to render
 *  and data which is an array of the same dimention with renderers. 
 *    this array contains nested array of values. Correcponding nested 
 *    array (rowGroup) will be rendered by correcponding renderer.
 *  e.g. 
 *    <Paginator renderers={renderers}
 *           pageHeight={400}
 *           pageWidth={800}
 *           data={data}
 *    />
 * 
 * The component received renderers and data in such format because it
 *  seems easy to get data in that format. In ternally, the data is
 *  transformed to structured that ease manipulation.
 *  e.g. when slicing the rows to find the max number of rows that fit in
 *  a page, the data, as an array of arrays, is transformed to array of
 *  individual row ({pageId, groupId, rowId}). When rendering, the array
 *  of rows is converted to an array of array of rowGroup ({groupId, startRowId, 
 *  endRowId}), where the outer array represent pages and the inner represent
 *  rowGroups in a single page.
 * For performance, the component implements binary search and debouncing.
 */
import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './style.css';

function renderDOM(reactElem, DOMNode) {
  return new Promise(resolve => {
    ReactDOM.render(reactElem, DOMNode, () => resolve());
  });
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

class Paginator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pagedRows: []
    };
    this.sizingRef = React.createRef();
    this.lastData = undefined;
    this.paginating = false;
    this.paginateID = 0;
  }

  render() {
    const {
      pageWidth,
      pageHeight,
      renderers,
      data
    } = this.props;
    const {
      pagedRows
    } = this.state;

    if (renderers.length !== data.length) {
      throw new Error('Paginator: renderer and data props should have the same dimension');
    }

    const validPagedRows = this.getValidRows(pagedRows, data);
    const pagedRowGroups = this.getPagedRowGroups(validPagedRows);
    return React.createElement(React.Fragment, null, pagedRowGroups.map((rowGroups, i) => {
      return React.createElement("div", {
        key: i,
        className: "Paginator__pageContainer",
        style: {
          width: pageWidth,
          height: pageHeight
        }
      }, rowGroups.map((rowGroup, j) => React.createElement(Fragment, {
        key: j
      }, renderers[rowGroup.groupId](data[rowGroup.groupId], rowGroup.startRowId, rowGroup.endRowId))));
    }), React.createElement("div", {
      className: "Paginator__pageContainer--hidden"
    }, React.createElement("div", {
      ref: this.sizingRef,
      style: {
        width: pageWidth
      }
    })));
  }

  componentDidMount() {
    if (this.lastData !== this.props.data) {
      this.lastData = this.props.data;
      this.paginate(0);
    }
  }

  componentDidUpdate() {
    if (this.lastData !== this.props.data) {
      this.lastData = this.props.data;
      this.paginate(500);
    }
  }

  getValidRows(pagedRows, data) {
    /* On data update, pagedRows may contain more row group than
     *  data does and in each group, more rows than data. This will
     *  result in index error. This trims pagedRows to fit data
     *  dimensions. such that, downstream use of pagedRows will
     *  not cause error.
     * This problem will quickly resolve after first render. pagination
     *  will fix pagedRows for the new data.
     */
    let tmpPagedRows = pagedRows.filter(rowObj => // trim away row group not in data
    rowObj.groupId < data.length // trim away row not in group
    && rowObj.rowId < data[rowObj.groupId].length); // Refill pageId, as above trim may remove all rows in a page,
    //  such that x page is missing.

    const pageNums = [...new Set(tmpPagedRows.map(row => row.pageId))];
    const oldToNewPageIdMap = pageNums.reduce((pageToNewPageMap, oldPageId, newPageId) => {
      pageToNewPageMap[oldPageId] = newPageId;
      return pageToNewPageMap;
    }, {});
    tmpPagedRows.forEach(row => {
      row.pageId = oldToNewPageIdMap[row.pageId];
    });
    return tmpPagedRows;
  }

  getPagedRowGroups(pagedRows) {
    /* Convert flattened array of row object with pageId, groupId, rowId
     *  to an array of array of rowGroup object,
     *  where the outer array representing pages and the inner array
     *  rowGroups object with groupId, startRowId, endRowId
     */
    const pagedRowGroups = pagedRows.reduce( // Groups rows: pageId, groupId, rowId
    //  into rowGroup: pageId, groupId, startRowId, endRowId 
    (rowGroups, rowObj) => {
      const lastRowGroup = rowGroups.pop() || {
        pageId: rowObj.pageId,
        groupId: rowObj.groupId,
        startRowId: rowObj.rowId,
        endRowId: rowObj.rowId + 1
      };

      if (lastRowGroup.pageId === rowObj.pageId && lastRowGroup.groupId === rowObj.groupId) {
        lastRowGroup.endRowId = rowObj.rowId + 1;
        rowGroups.push(lastRowGroup);
      } else {
        const newRowGroup = {
          pageId: rowObj.pageId,
          groupId: rowObj.groupId,
          startRowId: rowObj.rowId,
          endRowId: rowObj.rowId + 1
        };
        rowGroups.push(lastRowGroup, newRowGroup);
      }

      return rowGroups;
    }, []).reduce( // Put rowGroup from the same page into the same array
    (pagedRowGroups, rowGroup) => {
      pagedRowGroups[rowGroup.pageId] = pagedRowGroups[rowGroup.pageId] || [];
      pagedRowGroups[rowGroup.pageId].push(rowGroup);
      return pagedRowGroups;
    }, []);
    return pagedRowGroups;
  }

  async paginate(debounceTime) {
    /* Paginate data by rendering stuff in sizing container
     * We use binary search to find the pagination boundaries,
     *  where n rows underflows a page with pageHeight
     *  and n+1 rows would overflow.
     * First, we wait a little bit and
     *  wait futher if a pagination is ongoing
     *  or debounce if newer pagination is requested, save the effort
     *  or paginate if no pagination is ongoing, and
     *  no newer pagination is requested
     */
    this.paginateID += 1;
    let myPaginateID = this.paginateID; // Below,n while loop, debouncing

    while (true) {
      await sleep(debounceTime);

      if (this.paginating === true) {
        // previous pagination in progress
        // wait
        continue;
      } else if ( // this.paginating===false
      myPaginateID !== this.paginateID) {
        // another pagination requested
        // debounced
        return;
      } else if ( // this.paginating===false
      myPaginateID === this.paginateID) {
        // no pagination requested after this request started
        // i.e. not debounced
        break;
      } else {
        throw new Error('Proramming error: not supposed to reach this branch.');
      }
    } // start pagination


    this.paginating = true; // prevent another pagination instance

    const {
      pageHeight,
      data,
      renderers
    } = this.props;
    const sizingContainer = this.sizingRef.current;
    let unpagedRows = this.getUnpagedRowsFromData(data); // unwraps array of arrays to flattened array of object

    let pagedRows = [];

    for (let pageId = 0; unpagedRows.length !== 0; pageId++) {
      let start = 1; // at least 1 row per page

      let end = unpagedRows.length;
      let middle = Math.min(40, Math.floor((start + end) / 2)); // Heuristics: when assigning the initial middle, we starts with
      //  a biased binary search.
      // 40 rows of a 12pt text usually overflows a page and
      //  subsequent, unbiased binary seach will have a smaller space to
      //  search for.

      while (start <= end) {
        // Render rows start - middle (inclusive) into a single page
        const rowGroups = this.getPagedRowGroups(unpagedRows.slice(0, middle + 1))[0];
        await renderDOM(React.createElement(React.Fragment, null, rowGroups.map((rowGroup, i) => React.createElement(Fragment, {
          key: i
        }, renderers[rowGroup.groupId](data[rowGroup.groupId], rowGroup.startRowId, rowGroup.endRowId)))), sizingContainer); // Measure height

        let lastPageHeight = sizingContainer.getBoundingClientRect().height;

        if (lastPageHeight > pageHeight) {
          // if overflow
          // try move middle toward the start
          end = middle - 1;
          middle = Math.floor((start + end) / 2);
        } else {
          // if underflow
          // try move middle toward the end
          start = middle + 1;
          middle = Math.floor((start + end) / 2);
        }
      }

      let currentPageRows = unpagedRows.slice(0, start).map(row => ({ ...row,
        pageId: pageId
      }));
      pagedRows = pagedRows.concat(currentPageRows);
      unpagedRows = unpagedRows.slice(start);
    }

    this.paginating = false;
    this.setState({
      pagedRows
    });
  }

  getUnpagedRowsFromData(data) {
    /* Flattens a array of array of row values into
     *  array of objects that references row value by
     *  groupId, rowId.
     * pageId is set to 0 and is subject to change during actual
     *  pagination.
     * Flattened array is easier for pagination.
     * Empty inner arrays in data will result in an object of rowId=-1
     *  This allows the empty rowGroup still be rendered and
     *  participate in pagination
     * @param: data, array, an array of array of row values
     * @returns: array, an array of objects containing pageId,
     *  groupId, rowId
     */
    const unpagedRows = []; // unpaged, hence pageIndex always default to 0

    const pageId = 0;

    for (const [groupId, rowGroup] of data.entries()) {
      if (rowGroup.length === 0) {
        unpagedRows.push({
          pageId,
          groupId,
          rowId: -1
        });
      } // else


      for (const rowId of rowGroup.keys()) {
        unpagedRows.push({
          pageId,
          groupId,
          rowId
        });
      }
    }

    return unpagedRows;
  }

}

Paginator.propTypes = {
  pageWidth: PropTypes.number,
  pageHeight: PropTypes.number,
  renderers: PropTypes.arrayOf(PropTypes.func),
  data: PropTypes.arrayOf(PropTypes.array)
};
export default Paginator;
import React, {Component, PureComponent} from 'react';
import Paginator from './lib/Paginator';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      data:[
        [...new Array(25).keys()]
          .map(
            (value)=>value.toString()
          ),
        [...new Array(25).keys()]
          .map(
            (value)=>value.toString()
          ),
        ['This is the input','sadasd'],
        ['100px', '200px']
      ],
      renderers: [
        // 0
        (rows,start,end)=>(
          <DivGenerator0 rows={rows}
                         start={start}
                         end={end}
                         onChangeHandler={(i,changedValue)=>this.handleChange(0,i,changedValue)}
          />
        ),
        // 1
        (rows,start,end)=>(
          <InputGenerator0 rows={rows}
                           start={start}
                           end={end}
                           onChangeHandler={(i,changedValue)=>this.handleChange(1,i,changedValue)}
          />
        ),
        // 2
        (rows,start,end)=>(
          <TextBox rows={rows}
                         start={start}
                         end={end}
                         onChangeHandler={(i,changedValue)=>this.handleChange(2,i,changedValue)}
          />
        ),
        // 3
        (rows,start,end)=>(
          <TextBox1 rows={rows}
                   start={start}
                   end={end}
                   onChangeHandler={(i,changedValue)=>this.handleChange(3,i,changedValue)}
          />
        ),
      ]
    }
  }
  
  render(){
    const {data, renderers} = this.state;
    return (
      <>
        <Paginator renderers={renderers}
               pageHeight={400}
               pageWidth={600}
               data={data}
        />
        <Paginator renderers={[()=>'Forced pagination']}
               pageHeight={400}
               pageWidth={600}
               data={[[]]}
        />
        <Paginator renderers={[()=>'Forced pagination']}
               pageHeight={400}
               pageWidth={600}
               data={[[]]}
        />
        <Paginator
               renderers={[
                 (rows, start, end)=>
                   <div className='flex-container'>
                     {rows
                       .slice(start,end)
                       .map(
                         (v, i)=>
                           <div key={i}
                                className='flex-item'
                           >
                             {'a'.repeat(v)}
                           </div>
                       )
                     }
                   </div>
               ]}
               pageHeight={400}
               pageWidth={600}
               data={[[...new Array(50).keys()]]}
        />
      </>
    );
  }
  
  handleChange(groupId,rowId,changedValue){
    const newData = [...this.state.data];
    newData[groupId] = [...newData[groupId]];
    newData[groupId][rowId] = changedValue;
    this.setState({data: newData});
  }
}

function DivGenerator0(props){
  const {rows,start,end} = props;
  return (
    <div className='gen0'>
      {start===0
       ? <header>
          header on all pages
         </header>
       : <header>
          header on all pages cont'
         </header>
      }
      {rows.slice(start,end).map( (row,i)=>
        <div key={i}>{row}</div>
      )}
      {end===rows.length
       ? <footer>
           footer on all pages ({start}-{end}), and I know I am the last
         </footer>
       : <footer>
           footer on all pages ({start}-{end})
         </footer>
      }
    </div>
  );
}

function InputGenerator0(props){
  const {rows,start,end,onChangeHandler} = props;
  return (
    <div className='gen1'>
      {start===0
       ? <header>
          header on first page
         </header>
       : null
      }
      {rows.slice(start,end).map( (row,i)=>
        <div key={i}>
          <input value={row}
                 onChange={(ev)=>onChangeHandler(i+start, ev.target.value)}
                 />
        </div>
      )}
      {end===rows.length &&
        <footer>
          footer on last page
        </footer>
      }
    </div>
  );
}

class TextBox extends PureComponent {
  render(){
    const {rows,start,end,onChangeHandler} = this.props;
    return (
      <div className='gen2'>
        {start===0
         ? <header>
            header
           </header>
         : <header>
            header cont'
           </header>
        }
        {rows.slice(start,end).map( (row,i)=>
          <div key={i}>
            <textarea
              value={row}
              rows={10}
              style={{width:'100%', resize:'none', boxSizing:'border-box'}}
              onChange={(ev)=>onChangeHandler(i+start, ev.target.value)}
            />
          </div>
        )}
        {end===rows.length &&
          <footer>
            footer
          </footer>
        }
      </div>
    );
  }
}


class TextBox1 extends PureComponent {
  render(){
    const {rows,start,end,onChangeHandler} = this.props;
    return (
      <div className='gen2'>
        {start===0
         ? <header>
            header
           </header>
         : <header>
            header cont'
           </header>
        }
        {rows.slice(start,end).map( (row,i)=>
          <div key={i}>
            <textarea
              value={row}
              rows={10}
              style={{height:row, resize:'none', boxSizing:'border-box'}}
              onChange={(ev)=>onChangeHandler(i+start,ev.target.value)}
            />
          </div>
        )}
        {end===rows.length &&
          <footer>
            footer
          </footer>
        }
      </div>
    );
  }
}
export default App;
